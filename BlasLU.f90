program main
    implicit none
    integer dim, dim1, i, jj, j, k, flag, nb, jb, ip, info, iinfo, jp
    integer i1, i2, incx, k1, k2, inc, ix, ix0, n32, x, y, z
    real start_time, end_time, rand, one, small, sfmin
    real check, LU, LU1, normal, A1, zero, rnd, eps, temp
    integer isamax, m, n
    logical lsame
    parameter(one = 1.0e+0, zero = 0.0e+0)
    character char
    real, allocatable::A(:,:)
    integer, allocatable::B(:)
    real, allocatable::matr(:,:)
    external sgemm, sger, lsame, sswap, sscal, sgetf2, slaswp, strsm, isamax
    !intrinsic max, min, tiny, huge, epsilon
    print *, '__Enter dim: '
    read *, dim
    allocate (matr(dim,dim))
    allocate (A(dim,dim))
    allocate (B(dim))
    info = 0
    iinfo = 0
    rnd = one
    nb = 32
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            call RANDOM_NUMBER(rand)
            matr(i,j) = int(abs(rand * 10.0))
            j = j + 1
        enddo
        i = i + 1
    enddo
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            A(i,j) = matr(i,j)
            j = j + 1
        enddo
        i = i + 1
    enddo
    
    
    call CPU_TIME(start_time)
    do j = 1, dim, nb
        jb = min(dim - j + 1, nb)
        call sgemm('No transpose', 'No transpose', dim-j+1, jb, j-1, &
                   -one, A(j,1), dim, A(1,j), dim, one, A(j,j), dim)
        m = dim - j + 1
        n = jb
        if(one.EQ.rnd) then
            eps = epsilon(zero)*0.5
        else
            eps = epsilon(zero)
        endif
        sfmin = tiny(zero)
        small = one/huge(zero)
        if(small.GE.sfmin) then
            sfmin = small*(one+eps)
        endif
        do jj = 1, min(m,n)
            jp = jj - 1 + isamax(m-jj+1, A(jj,jj), 1)
            B(jj)=jp
            if(A(jp,jj).NE.zero) then
                if(jp.NE.jj) then
                    call sswap(n, A(jj,1), dim, A(jp,1), dim)
                endif
                if(jj.LT.m) then
                    if(abs(A(jj,jj)).GE.sfmin) then
                        call sscal(m-jj, one/A(jj,jj), &
                                   A(jj+1,jj), 1)
                    else
                        do x = 1, m-jj
                            A(jj+x,jj) = A(jj+x,jj) / &
                                                 A(jj,jj)
                        enddo
                    endif
                endif
            else if(iinfo.EQ.0) then
                iinfo = jj
            endif
            if(jj.LT.min(m,n)) then
                call sger(m-jj, n-jj, -one, A(jj+1,jj), 1, &
                          A(jj,jj+1), dim, A(jj+1,jj+1), dim)
            endif
        enddo 
        ix0 = j
        i1 = j
        i2 = j+jb-1
        inc = 1
        n32 = ((j-1)/32)*32
        if(n32.NE.0) then
            do x=1, n32, 32
                ix = ix0
                do y = i1, i2, inc
                    jp = B(ix)
                    if(ip.NE.i) then
                        do z = x, x + 31
                            temp = A(y,z)
                            A(y,z) = A(ip,z)
                            A(ip,z) = temp
                        enddo
                    endif
                    ix = ix + 1
                enddo
            enddo
        endif
        if(n32.NE.(j-1)) then
            n32 = n32 + 1
            ix = ix0
            do y = i1, i2, inc
                ip = B(ix)
                if(ip.NE.y) then
                    do z = n32, (j-1)
                        temp = A(y,z)
                        A(y,z) = A(ip,z)
                        A(ip,z) = temp    
                    enddo
                endif
                ix = ix + 1
            enddo
        endif 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if((j+jb).LE.dim) then
                ix0 = j
                i1 = j
                i2 = j+jb-1
                inc = 1
                n32 = ((dim-j-jb+1)/32)*32
                if(n32.NE.0) then
                    do x=1, n32, 32
                        ix = ix0
                        do y = i1, i2, inc
                            ip = B(ix)
                            if(ip.NE.i) then
                                do z = x, x + 31
                                    temp = A(y,z)
                                    A(y,z) = A(ip,z)
                                    A(ip,z) = temp
                                enddo
                            endif
                            ix = ix + 1
                        enddo
                    enddo
                endif
                if(n32.NE.(j-1)) then
                    n32 = n32 + 1
                    ix = ix0
                    do y = i1, i2, inc
                        ip = B(ix)
                        if(ip.NE.y) then
                            do z = n32, (dim-j-jb+1)
                                temp = A(y,z)
                                A(y,z) = A(ip,z)
                                A(ip,z) = temp
                            enddo
                        endif
                        ix = ix + 1
                    enddo
                endif
                call sgemm('No transpose', 'No transpose', jb, dim-j-jb+1, j-1, -one, &
                           A(j,j+1), dim, A(1,j+jb), dim, one, A(j,j+jb), dim)
                call strsm('Left', 'Lower', 'No transpose', 'Unit', jb, dim-j-jb+1, &
                           one, A(j,j), dim, A(j,j+jb), dim)
            endif
        enddo
    endif 
    call CPU_TIME(end_time)
    print *, 'Time of computing:', end_time - start_time, 'seconds'
    

!проверка на правильность
    flag = 1
    do while(flag == 1)
        print *,'Would you like to check for correctness?'
        print *,'__Press Y for Yes and N for No: '
        read *,char
        if(char.EQ.'Y'.OR.char.EQ.'y') then
            normal = 0
            LU = 0
            check = 0
            i = 1
            do while(i <= dim)
                j = 1
                do while(j <= dim)
                    LU1 = matr(i,j) - A(i,j)
                    LU=LU+LU1*LU1
                    A1 = matr(i,j)
                    normal=normal+A1*A1
                    j = j + 1
                enddo
                i = i + 1
            enddo
            if(normal.NE.0.AND.LU.NE.0) then
                print *,'Norm of A:', LU
                print *,'NORM OF MATR:', normal
                check = sqrt(LU)/sqrt(normal)
            endif
            if( check .EQ. 0 .OR. check .NE. check ) then
                print *,'Exact solution'
            else
                print *,'Check = ', check
            endif
            print *,'----'
            flag = 0
        else if(char.NE.'N'.AND.char.NE.'n') then
            print *,'Bad symbol'
            flag = 1
        else
            flag = 0
        endif
    enddo


!вывод в случае маленькой размерности
    if(dim > 10 ) then
        deallocate (A)
        deallocate (matr)
        goto 111
    endif
    
    print *, 'Initial matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *, matr(i,j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo
    print *, 'END matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *, A(i,j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo

    deallocate(A)
    deallocate(matr)
111 continue
end program main