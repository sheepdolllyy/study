program main
    implicit none
    integer dim, dim1, i, j, k, flag
    real start_time, end_time, rand
    real check, LU, LU1, normal, A1
    character char
    real, allocatable::A(:)
    integer, allocatable::B(:)
    real, allocatable::matr(:)
    EXTERNAL sgetrf
    print *, '__Enter dim: '
    read *, dim
    allocate (matr(dim*dim))
    allocate (A(dim * dim))
    allocate (B(dim * dim))
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            call RANDOM_NUMBER(rand)
            matr((i-1)*dim + j) = int(abs(rand * 10.0))
            j = j + 1
        enddo
        i = i + 1
    enddo
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            A((i-1)*dim + j) = matr((i-1)*dim + j)
            j = j + 1
        enddo
        i = i + 1
    enddo
    call CPU_TIME(start_time)
    call SGETRF(dim, dim, A, dim, B, flag)
    call CPU_TIME(end_time)
    print *, 'Time of computing:', end_time - start_time, 'seconds'
    

!проверка на правильность
    flag = 1
    do while(flag == 1)
        print *,'Would you like to check for correctness?'
        print *,'__Press Y for Yes and N for No: '
        read *,char
        if(char.EQ.'Y'.OR.char.EQ.'y') then
            normal = 0
            LU = 0
            check = 0
            i = 1
            do while(i <= dim)
                j = 1
                do while(j <= dim)
                    LU1 = matr((i-1)*dim + j) - A((i-1)*dim + j)
                    LU=LU+LU1*LU1
                    A1 = matr((i-1)*dim + j)
                    normal=normal+A1*A1
                    j = j + 1
                enddo
                i = i + 1
            enddo
            if(normal.NE.0.AND.LU.NE.0) then
                print *,'Norm of A:', LU
                print *,'NORM OF MATR:', normal
                check = sqrt(LU)/sqrt(normal)
            endif
            if( check .EQ. 0 .OR. check .NE. check ) then
                print *,'Exact solution'
            else
                print *,'Check = ', check
            endif
            print *,'----'
            flag = 0
        else if(char.NE.'N'.AND.char.NE.'n') then
            print *,'Bad symbol'
            flag = 1
        else
            flag = 0
        endif
    enddo


!вывод в случае маленькой размерности
    if(dim > 10 ) then
        deallocate (A)
        deallocate (matr)
        goto 111
    endif
    
    print *, 'Initial matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *, matr((i-1)*dim + j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo
    print *, 'END matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *, A((i-1)*dim + j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo

    deallocate(A)
    deallocate(matr)
111 continue
end program main