#include<stdlib.h>
#include<iostream>
#include <fstream>
#include<ctype.h>
#include"timer.h"
#include <pthread.h>
#include <cmath>

using namespace std;

#define NUM_THREADS 1

#define BlockSize 64
#define BlockGrainSize 100

enum task_t {
    WAIT,
    QUIT,
    CALC
};

struct ThreadInfo {
    task_t task;
    bool finished;

    int k_start  = 0;
    int k_finish = 0;

    int L_const_idx = 0;
    int U_const_idx = 0;

    const double *L = nullptr;
    const double *U = nullptr;

    double res;
};

pthread_mutex_t begin_mutex, end_mutex;
pthread_cond_t begin_cond, end_cond;

pthread_t thread[NUM_THREADS];
ThreadInfo data[NUM_THREADS];

void calc(ThreadInfo *thread_info) {
    const int k_start  = thread_info->k_start;
    const int k_finish = thread_info->k_finish;
    const int L_const_idx = thread_info->L_const_idx;
    const int U_const_idx = thread_info->U_const_idx;
    const double *L = thread_info->L;
    const double *U = thread_info->U;

    thread_info->res = 0;

    for (int k = k_start; k < k_finish; k++)
        thread_info->res += L[L_const_idx + k] * U[U_const_idx + k];
}

void *thread_routine(void *arg) {
    ThreadInfo *d = (ThreadInfo *)arg;
    task_t task;

    while (true) {
        pthread_mutex_lock(&begin_mutex);
        while (d->task == WAIT) {
            pthread_cond_wait(&begin_cond, &begin_mutex);
        }
        task = d->task;
        d->task = WAIT;
        pthread_mutex_unlock(&begin_mutex);

        switch (task) {
            case QUIT:
                return nullptr;
            case CALC:
                calc(d);

                pthread_mutex_lock(&end_mutex);
                d->finished = true;
                pthread_cond_signal(&end_cond);
                pthread_mutex_unlock(&end_mutex);
                break;
            default:
                break;
        }
    }
}

double parallel_sum_calculation(const double *L, const double *U,
                                int bias, int i, int j, int limit_value)
{
    const int granulation = (limit_value - bias) / NUM_THREADS;
    int last_thread_granulation = granulation;
    if (NUM_THREADS > limit_value)
        last_thread_granulation = limit_value;
    else if (limit_value % NUM_THREADS != 0)
        last_thread_granulation = limit_value + 1 - granulation * NUM_THREADS - bias;

    const int L_const_idx = i * (i + 1) / 2;
    const int U_const_idx = j * (j + 1) / 2;

    int current_k_start = bias;

    pthread_mutex_lock(&begin_mutex);
    for (int i = 0; i < NUM_THREADS; i++) {
        data[i].k_start  = current_k_start;
        data[i].k_finish = current_k_start + granulation;
        data[i].L_const_idx = L_const_idx;
        data[i].U_const_idx = U_const_idx;
        data[i].L = L;
        data[i].U = U;
        data[i].res = 0;

        if (i == NUM_THREADS - 1)
            data[i].k_finish = current_k_start + last_thread_granulation;

        data[i].task = CALC;
        data[i].finished = false;

        current_k_start += granulation;
    }
    pthread_cond_broadcast(&begin_cond);
    pthread_mutex_unlock(&begin_mutex);

    pthread_mutex_lock(&end_mutex);
    bool done;
    while (true) {
        done = true;
        for (int i = 0; i < NUM_THREADS; i++) {
            if (!data[i].finished) {
                done = false;
                break;
            }
        }
        if (done) {
            break;
        }
        pthread_cond_wait(&end_cond, &end_mutex);
    }
    double sum_value = 0;
    for (int i = 0; i < NUM_THREADS; i++) {
        sum_value += data[i].res;
    }
    pthread_mutex_unlock(&end_mutex);
    return sum_value;
}

void stop_threads() {
    pthread_mutex_lock(&begin_mutex);
    for (int i = 0; i < NUM_THREADS; i++) {
        data[i].task = QUIT;
    }
    pthread_cond_broadcast(&begin_cond);
    pthread_mutex_unlock(&begin_mutex);
}

void DiagonalSubmatrixLUDecompose(int bias, int squareSize, int matrixSize,
                                  const double *A, double *L, double *U) {
    int N = bias + squareSize;

    int i, j, k;
    double sum;

    L[bias*(bias + 3)/2] = 1.0;
    for (j = bias; j < N; j++) {
        U[j*(j + 1)/2 + bias] = A[bias*matrixSize + j];
    }

    for (i = bias + 1; i < N; i++) {
        L[i*(i + 1)/2 + bias] = A[i*matrixSize + bias]/U[bias*(bias + 3)/2];
        for (j = bias; j < i; j++) {
            sum = 0.0;
            for (k = bias; k < j; k++) {
                sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
            }
            L[i*(i + 1)/2 + j] = (A[i*matrixSize + j] - sum)/U[j*(j + 3)/2];
        }
        L[i*(i + 3)/2] = 1.0;
        for (j = i; j < N; j++) {
            sum = 0.0;
            for (k = bias; k < i; k++) {
                sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
            }
            U[j*(j + 1)/2 + i] = A[i*matrixSize + j] - sum;
        }
    }
}

void SolveUpperEquation(int bias, int squareSize, int matrixSize, const double
*A, const double *L, double *U) {
    int i, j, k;
    double sum;

    for (j = bias + squareSize; j < matrixSize; j++) {
        U[j*(j + 1)/2 + bias] = A[bias*matrixSize + j];
        for (i = bias + 1; i < bias + squareSize; i++) {
            sum = parallel_sum_calculation(L, U, bias, i, j, i);
            /*
            sum = 0.0;
			for (k = bias; k < i; k++) {
				sum += U[j*(j + 1)/2 + k]*L[i*(i + 1)/2 + k];
			}*/
            U[j*(j + 1)/2 + i] = A[i*matrixSize + j] - sum;
        }
    }
}

void SolveLeftEquation(int bias, int squareSize, int matrixSize, const double
*A, double *L, const double *U) {
    int i, j, k;
    double sum;

    for (i = bias + squareSize; i < matrixSize; i++) {
        L[i*(i + 1)/2 + bias] = A[i*matrixSize + bias]/U[bias*(bias +
                                                               3)/2];
        for (j = bias + 1; j < bias + squareSize; j++) {
            sum = parallel_sum_calculation(L, U, bias, i, j, j);
            /*sum = 0;
			for (k = bias; k < j; k++) {
				sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
			}*/
            L[i*(i + 1)/2 + j] = (A[i*matrixSize + j] - sum)/U[j*(j
                                                                  + 3)/2];
        }
    }
}

void UpdateDiagonalSubmatrix(int bias, int squareSize, int matrixSize, double
*A, double *L, double *U) {
    int i, j, k;
    double sum;

    for (i = bias + squareSize; i < matrixSize; i++) {
        for (j = bias + squareSize; j < matrixSize; j++) {
            sum = parallel_sum_calculation(L, U, bias, i, j, bias + squareSize);
            /*sum = 0;
			for (k = bias; k < bias + squareSize; k++) {
				sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
			}*/
            A[i*matrixSize + j] -= sum;
        }
    }
}

void LUDecompose(int matrixSize, double *A, double *L, double *U) {
    for (int bias = 0; bias < matrixSize; bias += BlockSize) {
        if (matrixSize - bias <= BlockGrainSize) {
            DiagonalSubmatrixLUDecompose(bias, matrixSize - bias,
                                         matrixSize, A, L, U);
            break;
        }
        DiagonalSubmatrixLUDecompose(bias, BlockSize, matrixSize, A, L,
                                     U);
        SolveUpperEquation(bias, BlockSize, matrixSize, A, L, U);
        SolveLeftEquation(bias, BlockSize, matrixSize, A, L, U);
        UpdateDiagonalSubmatrix(bias, BlockSize, matrixSize, A, L, U);
    }
}

void UpdateL(double *L, int N) {
    int k = 0;
    int a = 0;
    int i = N*N-1;
    while( i > 0 ) {
        L[i]=L[i-(N*N-N)/2+a];
        if(i%N == 0) {k++; i--;
            int j = 0;
            for(j; j < k; j++) {
                L[i-j] = 0;
                a++;
            }
            j--;
            i=i-j;
        }
        i--;
    }
}

void UpdateU(double *U, int N) {
    int k = 0;
    int a = 0;
    int i = N*N-1;
    while( i > 0 ) {
        U[i]=U[i-(N*N-N)/2+a];
        if(i%N == 0) {k++; i--;
            int j = 0;
            for(j; j < k; j++) {
                U[i-j] = 0;
                a++;
            }
            j--;
            i=i-j;
        }
        i--;
    }
    double *U1;
    U1 = new double [N*N];
    a = -1;
    for(int i = 0; i < N * N; i++) {
        if(i%N==0) {k=0; a++;}
        U1[i]=U[k*N+a];
        k++;
    }
    for(int i = 0; i < N*N; i++)
        U[i] = U1[i];
    delete[] U1;
}

int main() {
    pthread_mutex_init(&begin_mutex, nullptr);
    pthread_cond_init(&begin_cond, nullptr);
    pthread_mutex_init(&end_mutex, nullptr);
    pthread_cond_init(&end_cond, nullptr);

    for (int i = 0; i < NUM_THREADS; i++) {
        data[i].task = WAIT;
        data[i].finished = false;

        pthread_create(&thread[i], nullptr, thread_routine, &data[i]);
    }

    int dim;
    cout<<"Enter dim: ";
    cin>>dim;
    int dim1 = dim * dim;

    int *matr;
    double *L, *U;
    matr = new int[dim1];
    L = new double[dim1];
    U = new double[dim1];
    int i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            matr[dim * i + j] = rand() % 10;
            j++;
        }
        i++;
    }
    double *A;
    A = new double[dim1];
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim) {
            A[dim * i + j] = (double)matr[dim * i + j];
            j++;
        }
        i++;
    }
    i = 0;
    while( i < dim )
    {
        int j = 0;
        while( j < dim )
        {
            if( i == j ) L[dim * i + j] = 1;
            else L[dim * i + j] = 0;
            U[dim * i + j] = 0;
            j++;
        }
        i++;
    }

    cout<<" NUM_THREADS = "<< NUM_THREADS<<endl;

    Timer timer;
    timer.start();
    LUDecompose(dim, A, L, U);
    timer.stop();

    UpdateL(L, dim);
    UpdateU(U, dim);
    cout<<"--------------------\nalltime=";
    cout<<timer.getElapsed()<<"s\n------------------ --\n";
//________________________________

    stop_threads();

    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(thread[i], nullptr);
    }

    //проверка на правильность
    short tr = 1;
    while( tr ) {
        cout<<"Would you like to check for correctness?\nPress Y for 'Yes' and N for 'No': ";
        char ch;
        cin>>ch;
        if( ch == 'Y' || ch == 'y' ) {
            double a = 0, a1, lu1, lu = 0, check = 0;
            double *c;
            c = new double[dim1];
            int i = 0;
            while( i < dim ) {
                int j = 0;
                while( j < dim ) {
                    c[dim * i + j] = 0;
                    int k = 0;
                    while( k < dim ) {
                        c[dim * i + j] += L[dim * i + k] * U[dim * k + j];
                        k++;
                    }
                    j++;
                }
                i++;
            }
            i = 0;
            while( i < dim ) {
                int j = 0;
                while( j < dim ) {
                    lu1 = c[dim * i + j] - matr[dim * i + j];
                    lu += lu1 * lu1;
                    a1 = matr[dim * i + j];
                    a += a1 * a1;
                    j++;
                }
                i++;
            }
            if( a != 0 && lu != 0 )
                check = sqrt(lu) / sqrt(a);
            if( check == 0 || check != check )
                cout<<"--------------------\nexact solution!!!"<<endl;
            else cout<<"--------------------\ncheck = "<<check<<endl;
            cout<<"--------------------\n";
            tr = 0;
            delete[] c;
        } else if( ch != 'N' && ch != 'n' ) {
            cout<<"Bad symbol"<<endl;
            tr = 1;
        } else
            tr = 0;
    }

//вывод в случае мальнькой размерности
    if(dim > 10 ) {
        delete[] A;
        delete[] matr;
        delete[] U;
        delete[] L;
        return 0;
    }
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout<<(int)matr[dim * i + j]<<'\t';
            j++;
        }
        cout<<'\n';
        i++;
    }
    cout<<"\t------\n\t------";
    cout<<'\n';
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout.width(7);
            cout.precision(3);
            cout<<L[dim * i + j]<<' ';
            j++;
        }
        cout<<'\n';
        i++;
    }
    cout<<"\n\t"<<"\\  /\n\t \\/ \n\t /\\ \n\t/  \\\n";
    cout.width(5);
    cout.precision(3);
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout.width(7);
            cout.precision(3);
            cout<<U[dim * i + j]<<' ';
            j++;
        }
        cout<<'\n';
        i++;
    }
    delete[] A;
    delete[] matr;
    delete[] U;
    delete[] L;
    return 0;

    return 0;
}