program main
    implicit none
    integer dim, dim1, i, j, k, flag
    real start_time, end_time, rand
    real check, LU, LU1, normal, A1
    character char
    real, allocatable::A(:)
    real, allocatable::L(:)
    real, allocatable::U(:)
    real, allocatable::B(:)
    real, allocatable::matr(:)
    print *, '__Enter dim: '
    read *, dim
    call CPU_TIME(start_time)
    allocate (matr(dim*dim))
    allocate (A(dim * dim))
    allocate (L(dim * dim))
    allocate (U(dim * dim))
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            call RANDOM_NUMBER(rand)
            matr((i-1)*dim + j) = int(abs(rand * 10.0))
            j = j + 1
        enddo
        i = i + 1
    enddo
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            A((i-1)*dim + j) = matr((i-1)*dim + j)
            j = j + 1
        enddo
        i = i + 1
    enddo
    k = 1
    do while(k <= dim)
        U((k-1)*dim + k) = A((k-1)*dim + k)
        L((k-1)*dim + k) = 1
        i = k + 1
        do while(i <= dim)
            L((i-1)*dim + k) = A((i-1)*dim + k)/U((k-1)*dim + k)
            U((k-1)*dim + i)=A((k-1)*dim + i)
            i = i + 1
        enddo
        i = k + 1
        do while(i <= dim)
            j = k + 1
            do while(j <= dim)
                A((i-1)*dim + j)=A((i-1)*dim + j)-L((i-1)*dim + &
k)*U((k-1)*dim+j)
                j = j + 1
            enddo
            i = i + 1
        enddo
        k = k + 1
    enddo
    call CPU_TIME(end_time)
    print *, 'Time of computing:', end_time - start_time, 'seconds'

!проверка на правильность
    flag = 1
    do while(flag == 1)
        print *,'Would you like to check for correctness?'
        print *,'__Press Y for Yes and N for No: '
        read *,char
        if(char.EQ.'Y'.OR.char.EQ.'y') then
            normal = 0
            LU = 0
            check = 0
            allocate(B(dim * dim))
            i = 1
            do while(i <= dim)
                j = 1
                do while(j <= dim)
                    B((i-1)*dim + j) = 0
                    k = 1
                    do while(k <= dim)

                        B((i-1)*dim + j)=B((i-1)*dim + j)+L((i-1)*dim + &
k)/U((k-1)*dim + j)
                        k = k + 1
                    enddo
                    j = j + 1
                enddo
                i = i + 1
            enddo
            i = 1
            do while(i <= dim)
                j = 1
                do while(j <= dim)
                    LU1 = B((i-1)*dim + j) - A((i-1)*dim + j)
                    LU=LU+LU1*LU1
                    A1 = A((i-1)*dim + j)
                    normal=normal+A1*A1
                    j = j + 1
                enddo
                i = i + 1
            enddo
            if(normal.NE.0.AND.LU.NE.0) then

                check = sqrt(LU)/sqrt(normal)
            endif
            if( check .EQ. 0 .OR. check .NE. check ) then
                print *,'Exact solution'
            else
                print *,'Check = ', check
            endif
            print *,'----'
            flag = 0
            deallocate(B)
        else if(char.NE.'N'.AND.char.NE.'n') then
            print *,'Bad symbol'
            flag = 1
        else
            flag = 0
        endif
    enddo


!вывод в случае маленькой размерности
    if(dim > 10 ) then
        deallocate (A)
        deallocate (matr)
        deallocate (U)
        deallocate (L)
        goto 111
    endif
    print *, 'Initial matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *, matr((i-1)*dim + j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo
    print *,'L matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *,L((i-1)*dim + j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo
    print *, 'U matrix:'
    i = 1
    do while(i <= dim)
        j = 1
        do while(j <= dim)
            print *,U((i-1)*dim + j), '     '
            j = j + 1
        enddo
        print *,'_'
        i = i + 1
    enddo

    deallocate(A)
    deallocate(L)
    deallocate(U)
    deallocate(matr)
111 continue
end program main