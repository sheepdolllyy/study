#include<bits/stdc++.h>
#include<stdlib.h>
#include<iostream>
#include<ctype.h>
#include<time.h>

using namespace std;

int main() {
    int dim;
    cout<<"Enter dim: ";
    cin>>dim;
    int dim1 = dim * dim;
    clock_t stime = clock();
    int *matr;
    double *L, *U;
    matr = new int[dim1];
    L = new double[dim1];
    U = new double[dim1];
    int i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            matr[dim * i + j] = rand() % 10;
            j++;
        }
        i++;
    }
    double *A;
    A = new double[dim1];
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            A[dim * i + j] = (double)matr[dim * i + j];
            j++;
        }
        i++;
    }
    i = 0;
    while( i < dim )
    {
        int j = 0;
        while( j < dim )
        {
            if( i == j ) L[dim * i + j] = 1;
            else L[dim * i + j] = 0;
            U[dim * i + j] = 0;
            j++;
        }
        i++;
    }
    int k = 0;
    while( k < dim ) {
        U[dim * k + k] = A[dim * k + k];
        int i = k + 1;
        while( i < dim ) {
            L[dim * i + k] = A[dim * i + k] / U[dim * k + k];
            U[dim * k + i] = A[dim * k + i];
            i++;
        }
        i = k + 1;
        while( i < dim ) {
            int j = k + 1;
            while( j < dim ) {
                A[dim * i + j] = A[dim * i + j] - L[dim * i + k] * U[dim * k + j];
                j++;
            }
            i++;
        }
        k++;
    }
    clock_t etime = clock();
    cout<<"--------------------\nalltime = "<<(double)(etime - stime) / CLOCKS_PER_SEC<<"s\n--------------------\n";
//________________________________



//проверка на правильность
    short tr = 1;
    while( tr ) {
      cout<<"Would you like to check for correctness?\nPress Y for 'Yes' and N for 'No': ";
      char ch;
      cin>>ch;
      if( ch == 'Y' || ch == 'y' ) {
        double a = 0, a1, lu1, lu = 0, check = 0;
        double *c;
        c = new double[dim1];
        int i = 0;
        while( i < dim ) {
            int j = 0;
            while( j < dim ) {
                c[dim * i + j] = 0;
                int k = 0;
                while( k < dim ) {
                    c[dim * i + j] += L[dim * i + k] * U[dim * k + j];
                    k++;
                }
                j++;
            }
            i++;
        }
        i = 0;
        while( i < dim ) {
            int j = 0;
            while( j < dim ) {
                lu1 = c[dim * i + j] - matr[dim * i + j];
                lu += lu1 * lu1;
                a1 = matr[dim * i + j];
                a += a1 * a1;
                j++;
            }
            i++;
        }
        if( a != 0 && lu != 0 )
            check = sqrt(lu) / sqrt(a);
        if( check == 0 || check != check )
            cout<<"--------------------\nexact solution!!!"<<endl;
        else cout<<"--------------------\ncheck = "<<check<<endl;
        cout<<"--------------------\n";
        tr = 0;
        delete[] c;
      } else if( ch != 'N' && ch != 'n' ) {
        cout<<"Bad symbol"<<endl;
        tr = 1;
      } else
        tr = 0;
    }


//вывод в случае мальнькой размерности
    if(dim > 10 ) {
        delete[] A;
        delete[] matr;
        delete[] U;
        delete[] L;
        return 0;
    }
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout<<(int)matr[dim * i + j]<<'\t';
            j++;
        }
        cout<<'\n';
        i++;
    }
    cout<<"\t------\n\t------";
    cout<<'\n';
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout.width(7);
            cout.precision(3);
            cout<<L[dim * i + j]<<' ';
            j++;
        }
        cout<<'\n';
        i++;
    }
    cout<<"\n\t"<<"\\  /\n\t \\/ \n\t /\\ \n\t/  \\\n";
    cout.width(5);
    cout.precision(3);
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout.width(7);
            cout.precision(3);
            cout<<U[dim * i + j]<<' ';
            j++;
        }
        cout<<'\n';
        i++;
    }
    delete[] A;
    delete[] matr;
    delete[] U;
    delete[] L;
    return 0;
}