#include<bits/stdc++.h>
#include<stdlib.h>
#include<iostream>
#include <fstream>
#include<ctype.h>
#include<time.h>
#include <omp.h>
#include "timer.h"

using namespace std;

#define BlockSize 64
#define BlockGrainSize 100  

void DiagonalSubmatrixLUDecompose(int bias, int squareSize, int matrixSize, 
const double *A, double *L, double *U) {
	int N = bias + squareSize;

	int i, j, k;
	double sum;

	L[bias*(bias + 3)/2] = 1.0;
	for (j = bias; j < N; j++) {
		U[j*(j + 1)/2 + bias] = A[bias*matrixSize + j];
	}

	for (i = bias + 1; i < N; i++) {
		L[i*(i + 1)/2 + bias] = A[i*matrixSize + bias]/U[bias*(bias + 3)/2];
		for (j = bias; j < i; j++) {
			sum = 0.0;
			for (k = bias; k < j; k++) {
				sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
			}
			L[i*(i + 1)/2 + j] = (A[i*matrixSize + j] - sum)/U[j*(j + 3)/2];
		}
		L[i*(i + 3)/2] = 1.0;
		for (j = i; j < N; j++) {
			sum = 0.0;
			for (k = bias; k < i; k++) {
				sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
			}
			U[j*(j + 1)/2 + i] = A[i*matrixSize + j] - sum;
		}
	}
}

void SolveUpperEquation(int bias, int squareSize, int matrixSize, const double 
*A, const double *L, double *U) {
#pragma omp parallel for
	for (int j = bias + squareSize; j < matrixSize; j++) {
		U[j*(j + 1)/2 + bias] = A[bias*matrixSize + j];
		for (int i = bias + 1; i < bias + squareSize; i++) {
			double sum = 0.0;
			for (int k = bias; k < i; k++) {
				sum += U[j*(j + 1)/2 + k]*L[i*(i + 1)/2 + k];
			}
			U[j*(j + 1)/2 + i] = A[i*matrixSize + j] - sum;
		}
	}
}

void SolveLeftEquation(int bias, int squareSize, int matrixSize, const double 
*A, double *L, const double *U) {
#pragma omp parallel for
	for (int i = bias + squareSize; i < matrixSize; i++) {
		L[i*(i + 1)/2 + bias] = A[i*matrixSize + bias]/U[bias*(bias + 
3)/2];
		for (int j = bias + 1; j < bias + squareSize; j++) {
			double sum = 0;
			for (int k = bias; k < j; k++) {
				sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
			}
			L[i*(i + 1)/2 + j] = (A[i*matrixSize + j] - sum)/U[j*(j 
+ 3)/2];
		}
	}
}

void UpdateDiagonalSubmatrix(int bias, int squareSize, int matrixSize, double 
*A, double *L, double *U) {
#pragma omp parallel for
	for (int i = bias + squareSize; i < matrixSize; i++) {
		for (int j = bias + squareSize; j < matrixSize; j++) {
			double sum = 0;
			for (int k = bias; k < bias + squareSize; k++) {
				sum += L[i*(i + 1)/2 + k]*U[j*(j + 1)/2 + k];
			}
			A[i*matrixSize + j] -= sum;
		}
	}
}

void LUDecompose(int matrixSize, double *A, double *L, double *U) {
	for (int bias = 0; bias < matrixSize; bias += BlockSize) {
		if (matrixSize - bias <= BlockGrainSize) {
			DiagonalSubmatrixLUDecompose(bias, matrixSize - bias, 
matrixSize, A, L, U);
			break;
		}
		DiagonalSubmatrixLUDecompose(bias, BlockSize, matrixSize, A, L, 
U);
        //#pragma omp parallel sections 
        {
          //  #pragma omp section 
            {
                SolveUpperEquation(bias, BlockSize, matrixSize, A, L, U);
            }
            //#pragma omp section
            {
                SolveLeftEquation(bias, BlockSize, matrixSize, A, L, U);
            }
        }
        UpdateDiagonalSubmatrix(bias, BlockSize, matrixSize, A, L, U);
    }
}

void UpdateL(double *L, int N) {
   int k = 0;
   int a = 0;
   int i = N*N-1;
   while( i > 0 ) {
       L[i]=L[i-(N*N-N)/2+a];
       if(i%N == 0) {k++; i--;
           int j = 0;
           for(j; j < k; j++) {
               L[i-j] = 0;
               a++;
           }
           j--;
           i=i-j;
        }
        i--;
    }
}

void UpdateU(double *U, int N) {
    int k = 0;
    int a = 0;
    int i = N*N-1;
    while( i > 0 ) {
        U[i]=U[i-(N*N-N)/2+a];
        if(i%N == 0) {k++; i--;
            int j = 0;
            for(j; j < k; j++) {
                U[i-j] = 0;
                a++;
            }
            j--;
            i=i-j;
        }
        i--;
    }
    double *U1;
    U1 = new double [N*N];
    a = -1;
    for(int i = 0; i < N * N; i++) {
        if(i%N==0) {k=0; a++;}
        U1[i]=U[k*N+a];
        k++;
    }
    for(int i = 0; i < N*N; i++)
        U[i] = U1[i];
    delete[] U1;
}

int main() {
    int dim;
    cout<<"Enter dim: ";
    cin>>dim;
    int dim1 = dim * dim;

    int *matr;
    double *L, *U;
    matr = new int[dim1];
    L = new double[dim1];
    U = new double[dim1];
    int i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            matr[dim * i + j] = rand() % 10;
            j++;
        }
        i++;
    }
    double *A;
    A = new double[dim1];
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim) {
            A[dim * i + j] = (double)matr[dim * i + j];
            j++;
        }
        i++;
    }
    i = 0;
    while( i < dim )
    {
        int j = 0;
        while( j < dim )
        {
            if( i == j ) L[dim * i + j] = 1;
            else L[dim * i + j] = 0;
            U[dim * i + j] = 0;
            j++;
        }
        i++;
    }
    
    //omp_set_nested(1);
   
    Timer timer;
    timer.start();
    LUDecompose(dim, A, L, U);
    timer.stop();
    
    UpdateL(L, dim);
    UpdateU(U, dim);
    cout<<"--------------------\nalltime=";
    cout<<timer.getElapsed()<<"s\n------------------ --\n";
//________________________________



//проверка на правильность
    short tr = 1;
    while( tr ) {
      cout<<"Would you like to check for correctness?\nPress Y for 'Yes' and N for 'No': ";
      char ch;
      cin>>ch;
      if( ch == 'Y' || ch == 'y' ) {
        double a = 0, a1, lu1, lu = 0, check = 0;
        double *c;
        c = new double[dim1];
        int i = 0;
        while( i < dim ) {
            int j = 0;
            while( j < dim ) {
                c[dim * i + j] = 0;
                int k = 0;
                while( k < dim ) {
                    c[dim * i + j] += L[dim * i + k] * U[dim * k + j];
                    k++;
                }
                j++;
            }
            i++;
        }
        i = 0;
        while( i < dim ) {
            int j = 0;
            while( j < dim ) {
                lu1 = c[dim * i + j] - matr[dim * i + j];
                lu += lu1 * lu1;
                a1 = matr[dim * i + j];
                a += a1 * a1;
                j++;
            }
            i++;
        }
        if( a != 0 && lu != 0 )
            check = sqrt(lu) / sqrt(a);
        if( check == 0 || check != check )
            cout<<"--------------------\nexact solution!!!"<<endl;
        else cout<<"--------------------\ncheck = "<<check<<endl;
        cout<<"--------------------\n";
        tr = 0;
        delete[] c;
      } else if( ch != 'N' && ch != 'n' ) {
        cout<<"Bad symbol"<<endl;
        tr = 1;
      } else
        tr = 0;
    }

//вывод в случае мальнькой размерности
    if(dim > 10 ) {
        delete[] A;
        delete[] matr;
        delete[] U;
        delete[] L;
        return 0;
    }
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout<<(int)matr[dim * i + j]<<'\t';
            j++;
        }
        cout<<'\n';
        i++;
    }
    cout<<"\t------\n\t------";
    cout<<'\n';
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout.width(7);
            cout.precision(3);
            cout<<L[dim * i + j]<<' ';
            j++;
        }
        cout<<'\n';
        i++;
    }
    cout<<"\n\t"<<"\\  /\n\t \\/ \n\t /\\ \n\t/  \\\n";
    cout.width(5);
    cout.precision(3);
    i = 0;
    while( i < dim ) {
        int j = 0;
        while( j < dim ) {
            cout.width(7);
            cout.precision(3);
            cout<<U[dim * i + j]<<' ';
            j++;
        }
        cout<<'\n';
        i++;
    }
    delete[] A;
    delete[] matr;
    delete[] U;
    delete[] L;
    return 0;
}